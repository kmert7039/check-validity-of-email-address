
//https://help.returnpath.com/hc/en-us/articles/220560587-What-are-the-rules-for-email-address-syntax-


#include "Modules.h"
#include <string.h>

//Check for None Alphabet && Digit character
int checkSpecialCharacter(char ch) {
    if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
        return 0; // Alphabet
    } else if (ch >= '0' && ch <= '9') {
        return 0; //Digit
    } else {
        return 1; // None Alphabet && Digit
    }
}
void appMain(int argc,char *argv[]){
    printf("Mail Validator\n");
    printf("Made by: Kaan Bilgihan Mert \nNeptun Code:I82P4C\n\n");
    if( argc == 2 ) {
     if(!argv[1])
     {
         printf("Please enter your e-mail address!\n");
         return;
     }
     char *userMail = argv[1];
     int mailLength = 0;

        int atCount = 0;
        int dotCount = 0;
        for (int curPos = 0; userMail[curPos] != '\0'; curPos++) { //removed strlen
            mailLength++;
            //Check for @ and . count
            if (userMail[curPos] == '@') {
                atCount++;
            }
            if (userMail[curPos] == '.') {
                dotCount++;
            }


            /*
            if I remove this if the code will fail. This is because I am looking duplicated characters.
             Current char + 1 The loop goes until character = \0 it is not checking char+1 so if mail is 32 character
             it will only check 32 characters not 33, but because of duplication check
             when it hits 32nd character it will check 33th character but it is \0 so it will think it is special character that is why Im checking if curPos+1 != \0
             */
            if(curPos+1!='\0'){
                if ( checkSpecialCharacter(userMail[curPos]) && checkSpecialCharacter(userMail[curPos + 1])) { //if char is special
                    printf("ERROR: The special character must not be accompanied by another special character!\n");
                    printf("Email address verification failed. Your e-mail address is not valid!\n");
                    return;
                }
            }
        }

        if(atCount==1 && dotCount >= 1){
            //Check special character on first character and last character
            if(!checkSpecialCharacter(userMail[0]) && !checkSpecialCharacter(userMail[mailLength - 1])){
                //check duplicated char
                    printf("Email address verification successful. Your e-mail address is valid!\n");
                    return;
            }else{
                printf("ERROR: Your e-mail address cannot start or end with a special character!\n");
                printf("Email address verification failed. Your e-mail address is not valid!\n");
                return;
            }
        }else{
            printf("ERROR: Your e-mail address does not reach the required '@' or '.' count!\n");
            printf("Email address verification failed. Your e-mail address is not valid!\n");
            return;
        }
    }
    else if( argc > 2 ) {
        printf("Too many arguments supplied.\n");
        return;
    }
    else {
        printf("Please enter your e-mail address!\n");
        return;
    }
}
